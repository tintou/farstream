option('farstream-plugins', type : 'array', choices : ['fsrawconference', 'fsrtpconference', 'fsvideoanyrate', 'fsrtpxdata'], value : ['fsrawconference', 'fsrtpconference', 'fsvideoanyrate', 'fsrtpxdata'], description : 'GStreamer plugins to build')
option('farstream-transmitters', type : 'array', choices : ['rawudp', 'multicast', 'nice', 'shm'], value : ['rawudp', 'multicast', 'nice', 'shm'], description : 'Transmitter plugins to build')
option('gtk-doc', type : 'boolean', description : 'Build the documentation')
option('gupnp', type : 'boolean', description : 'Build with GUPnP support')
