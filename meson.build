project('farstream', 'c', version: '0.2.8.1')

farstream_api_version = '0.2'
plugin_path = join_paths (get_option('libdir'), 'farstream-'+farstream_api_version)

gnome = import('gnome')

nice_dep = dependency('nice', version: '>=0.1.8')

glib_min_version = '2.32'
glib_dep = dependency('glib-2.0', version: '>='+glib_min_version)
gmodule_dep = dependency('gmodule-2.0', version: '>='+glib_min_version)
gio_dep = dependency('gio-2.0', version: '>='+glib_min_version)
gio_unix_dep = dependency('gio-unix-2.0', version: '>='+glib_min_version, required: false)
gst_dep = dependency('gstreamer-1.0')
gst_base_dep = dependency('gstreamer-base-1.0')
gst_plugins_base_dep = dependency('gstreamer-plugins-base-1.0')
gst_rtp_dep = dependency('gstreamer-rtp-1.0')
gst_check_dep = dependency('gstreamer-check-1.0', required: false)
libm_dep = meson.get_compiler('c').find_library('m', required : false)

gupnp_dep = []
if get_option('gupnp')
    gupnp_dep = dependency('gupnp-1.0')
endif


top_dir = include_directories('.')

add_project_arguments('-DHAVE_CONFIG_H=1', language: 'c')
config_data = configuration_data()

# Ensure that we remain compatible vith the glib_min_version by ignoring future deprecation warnings and prevent the use of post minimal version API
glib_min_version_symbol = 'GLIB_VERSION_'+'_'.join(glib_min_version.split('.'))
config_data.set('GLIB_VERSION_MIN_REQUIRED', glib_min_version_symbol)
config_data.set('GLIB_VERSION_MAX_ALLOWED', glib_min_version_symbol)

config_data.set_quoted('PACKAGE', meson.project_name())
config_data.set_quoted('VERSION', meson.project_version())
config_data.set_quoted('PACKAGE_VERSION', meson.project_version())

if get_option('gupnp')
    config_data.set('HAVE_GUPNP', '1')
endif

config_data.set_quoted('HOST_CPU', host_machine.cpu_family())
config_data.set_quoted('GST_API_VERSION', '1.0')
config_data.set_quoted('FS_PLUGIN_PATH', join_paths (get_option('prefix'), plugin_path))
config_data.set_quoted('FS_APIVERSION', farstream_api_version)
config_header = configure_file(
    configuration: config_data,
    output: 'config.h'
)

subdir('farstream')
subdir('gst')
subdir('transmitters')
subdir('examples')
subdir('tests')
